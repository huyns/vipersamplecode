// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Account.m instead.

#import "_Account.h"

@implementation AccountID
@end

@implementation _Account

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Account" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Account";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Account" inManagedObjectContext:moc_];
}

- (AccountID*)objectID {
	return (AccountID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic address;

@dynamic name;

@dynamic phone;

@dynamic website;

@dynamic contacts;

- (NSMutableOrderedSet<Contact*>*)contactsSet {
	[self willAccessValueForKey:@"contacts"];

	NSMutableOrderedSet<Contact*> *result = (NSMutableOrderedSet<Contact*>*)[self mutableOrderedSetValueForKey:@"contacts"];

	[self didAccessValueForKey:@"contacts"];
	return result;
}

@end

@implementation _Account (ContactsCoreDataGeneratedAccessors)
- (void)addContacts:(NSOrderedSet<Contact*>*)value_ {
	[self.contactsSet unionOrderedSet:value_];
}
- (void)removeContacts:(NSOrderedSet<Contact*>*)value_ {
	[self.contactsSet minusOrderedSet:value_];
}
- (void)addContactsObject:(Contact*)value_ {
	[self.contactsSet addObject:value_];
}
- (void)removeContactsObject:(Contact*)value_ {
	[self.contactsSet removeObject:value_];
}
- (void)insertObject:(Contact*)value inContactsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"contacts"];
}
- (void)removeObjectFromContactsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"contacts"];
}
- (void)insertContacts:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"contacts"];
}
- (void)removeContactsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"contacts"];
}
- (void)replaceObjectInContactsAtIndex:(NSUInteger)idx withObject:(Contact*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"contacts"];
}
- (void)replaceContactsAtIndexes:(NSIndexSet *)indexes withContacts:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"contacts"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self contacts]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"contacts"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"contacts"];
}
@end

@implementation AccountAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)phone {
	return @"phone";
}
+ (NSString *)website {
	return @"website";
}
@end

@implementation AccountRelationships 
+ (NSString *)contacts {
	return @"contacts";
}
@end

