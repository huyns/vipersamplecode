// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Account.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Contact;

@interface AccountID : NSManagedObjectID {}
@end

@interface _Account : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AccountID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone;

@property (nonatomic, strong, nullable) NSString* website;

@property (nonatomic, strong, nullable) NSOrderedSet<Contact*> *contacts;
- (nullable NSMutableOrderedSet<Contact*>*)contactsSet;

@end

@interface _Account (ContactsCoreDataGeneratedAccessors)
- (void)addContacts:(NSOrderedSet<Contact*>*)value_;
- (void)removeContacts:(NSOrderedSet<Contact*>*)value_;
- (void)addContactsObject:(Contact*)value_;
- (void)removeContactsObject:(Contact*)value_;

- (void)insertObject:(Contact*)value inContactsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContactsAtIndex:(NSUInteger)idx;
- (void)insertContacts:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContactsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContactsAtIndex:(NSUInteger)idx withObject:(Contact*)value;
- (void)replaceContactsAtIndexes:(NSIndexSet *)indexes withContacts:(NSArray *)values;

@end

@interface _Account (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;

- (NSString*)primitiveWebsite;
- (void)setPrimitiveWebsite:(NSString*)value;

- (NSMutableOrderedSet<Contact*>*)primitiveContacts;
- (void)setPrimitiveContacts:(NSMutableOrderedSet<Contact*>*)value;

@end

@interface AccountAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)name;
+ (NSString *)phone;
+ (NSString *)website;
@end

@interface AccountRelationships: NSObject
+ (NSString *)contacts;
@end

NS_ASSUME_NONNULL_END
