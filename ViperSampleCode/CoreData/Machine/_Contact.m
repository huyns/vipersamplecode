// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Contact.m instead.

#import "_Contact.h"

@implementation ContactID
@end

@implementation _Contact

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Contact";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:moc_];
}

- (ContactID*)objectID {
	return (ContactID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic dob;

@dynamic email;

@dynamic first_name;

@dynamic last_name;

@dynamic phone;

@dynamic account;

@end

@implementation ContactAttributes 
+ (NSString *)dob {
	return @"dob";
}
+ (NSString *)email {
	return @"email";
}
+ (NSString *)first_name {
	return @"first_name";
}
+ (NSString *)last_name {
	return @"last_name";
}
+ (NSString *)phone {
	return @"phone";
}
@end

@implementation ContactRelationships 
+ (NSString *)account {
	return @"account";
}
@end

