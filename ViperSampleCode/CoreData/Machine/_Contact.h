// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Contact.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Account;

@interface ContactID : NSManagedObjectID {}
@end

@interface _Contact : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ContactID *objectID;

@property (nonatomic, strong, nullable) NSString* dob;

@property (nonatomic, strong, nullable) NSString* email;

@property (nonatomic, strong, nullable) NSString* first_name;

@property (nonatomic, strong, nullable) NSString* last_name;

@property (nonatomic, strong, nullable) NSString* phone;

@property (nonatomic, strong, nullable) Account *account;

@end

@interface _Contact (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDob;
- (void)setPrimitiveDob:(NSString*)value;

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSString*)primitiveFirst_name;
- (void)setPrimitiveFirst_name:(NSString*)value;

- (NSString*)primitiveLast_name;
- (void)setPrimitiveLast_name:(NSString*)value;

- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;

- (Account*)primitiveAccount;
- (void)setPrimitiveAccount:(Account*)value;

@end

@interface ContactAttributes: NSObject 
+ (NSString *)dob;
+ (NSString *)email;
+ (NSString *)first_name;
+ (NSString *)last_name;
+ (NSString *)phone;
@end

@interface ContactRelationships: NSObject
+ (NSString *)account;
@end

NS_ASSUME_NONNULL_END
