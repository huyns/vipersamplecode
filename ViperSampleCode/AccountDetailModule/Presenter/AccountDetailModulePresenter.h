//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDetailModuleProtocols.h"
#import "AccountDetailModuleViewModel.h"

@class AccountDetailModuleWireFrame;

@interface AccountDetailModulePresenter : NSObject <AccountDetailModulePresenterProtocol, AccountDetailModuleInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountDetailModuleViewProtocol> view;
@property (nonatomic, strong) AccountDetailModuleViewModel * viewModel;
@property (nonatomic, strong) id <AccountDetailModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <AccountDetailModuleWireFrameInputProtocol> wireFrame;

@end
