//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModulePresenter.h"
#import "AccountDetailModuleWireframe.h"

@implementation AccountDetailModulePresenter

#pragma mark AccountDetailModulePresenterProtocol
-(void) updateView
{
    [self.view updateAccountInfor];
    [self.interactor findAllContactByAccount:self.viewModel.accountModel];
}

-(void) addContact
{
    [self.wireFrame presentAddContactViewForAccount:self.viewModel.accountModel];
}

-(void) showContactAtIndexPath:(NSIndexPath *) indexPath
{
    [self.wireFrame presentContactDetail:self.viewModel.contacts[indexPath.row]];
}

#pragma mark AccountDetailModuleInteractorOutputProtocol
-(void) foundContacts:(NSArray *) contacts
{
    [self.viewModel setContacts:contacts];
    [self.view reloadContactList];
}

-(void) dealloc
{
    NSLog(@"AccountDetailModulePresenter dealloc");
}
@end