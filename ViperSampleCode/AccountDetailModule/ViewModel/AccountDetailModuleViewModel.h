//
//  AccountDetailModuleViewModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountModel.h"

@interface AccountDetailModuleViewModel : NSObject

@property (nonatomic, strong) AccountModel *accountModel;
@property (nonatomic, strong) NSArray *contacts;

-(NSString *) accountName;
-(NSString *) accountAddress;
-(NSString *) accountPhoneNumber;
-(NSString *) accountWebsite;

-(NSString *) titleForHeaderView;
-(NSInteger) numberOfRowInSection:(NSInteger) section;
-(NSString *) titleForItemAtIndexPath:(NSIndexPath *) indexPath;
-(NSString *) subTitleForItemAtIndexPath:(NSIndexPath *) indexPath;

@end
