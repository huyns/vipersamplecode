//
//  AccountDetailModuleViewModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModuleViewModel.h"
#import <UIKit/UIKit.h>
#import "ContactModel.h"

@implementation AccountDetailModuleViewModel

-(NSString *) accountName
{
    return self.accountModel.name;
}

-(NSString *) accountAddress
{
    return self.accountModel.address;
}

-(NSString *) accountPhoneNumber
{
    return self.accountModel.phone;
}

-(NSString *) accountWebsite
{
    return self.accountModel.website;
}

-(NSString *) titleForHeaderView
{
    return [NSString stringWithFormat:@"Contacts (%lu)",(unsigned long)self.contacts.count];
}

-(NSInteger) numberOfRowInSection:(NSInteger) section
{
    return self.contacts.count;
}

-(NSString *) titleForItemAtIndexPath:(NSIndexPath *) indexPath
{
    ContactModel *contactModel = [self.contacts objectAtIndex:indexPath.row];
    return [NSString stringWithFormat:@"%@ %@",contactModel.firstName, contactModel.lastName];
}

-(NSString *) subTitleForItemAtIndexPath:(NSIndexPath *) indexPath
{
    ContactModel *contactModel = [self.contacts objectAtIndex:indexPath.row];
    return contactModel.email;
}

@end
