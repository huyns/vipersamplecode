//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDetailModuleProtocols.h"


@interface AccountDetailModuleInteractor : NSObject <AccountDetailModuleInteractorInputProtocol, AccountDetailModuleDataManagerOutputProtocol, AccountDetailModuleServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountDetailModuleInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <AccountDetailModuleDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <AccountDetailModuleServiceManagerInputProtocol> serviceManager;

@end