//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDetailModuleProtocols.h"


@interface AccountDetailModuleServiceManager : NSObject
    <AccountDetailModuleServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountDetailModuleServiceManagerOutputProtocol> interactor;

@end