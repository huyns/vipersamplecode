//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModuleDataManager.h"
#import "AccountModel.h"
#import "ContactModel.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation AccountDetailModuleDataManager

#pragma mark AccountDetailModuleDataManagerInputProtocol
-(void) fetchAllContactByAccount:(AccountModel *)account
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account.name == %@",account.name];
    NSArray *contacts = [Contact MR_findAllWithPredicate:predicate
                                               inContext:[NSManagedObjectContext MR_defaultContext]];
    [self.interactor fetchedContacts:[ContactModel contactObjectsFromContactEntities:contacts]];
}

@end