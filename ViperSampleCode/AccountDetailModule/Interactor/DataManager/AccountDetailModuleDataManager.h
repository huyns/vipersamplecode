//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDetailModuleProtocols.h"


@interface AccountDetailModuleDataManager : NSObject <AccountDetailModuleDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountDetailModuleDataManagerOutputProtocol> interactor;

@end