//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModuleInteractor.h"

@implementation AccountDetailModuleInteractor

#pragma mark AccountDetailModuleInteractorInputProtocol
-(void) findAllContactByAccount:(id) account
{
    [self.dataManager fetchAllContactByAccount:account];
}

#pragma mark AccountDetailModuleDataManagerOutputProtocol
-(void) fetchedContacts:(NSArray *) contactModels
{
    [self.presenter foundContacts:contactModels];
}

@end