//
//  AccountDetaiModuleHeaderView.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountDetaiModuleHeaderView : UITableViewHeaderFooterView

@property (nonatomic, copy) void (^ didTapAddNewContact) (id sender);
@property (weak, nonatomic) IBOutlet UILabel *contactCountLabel;
- (IBAction)onAddNewContact:(id)sender;

@end
