//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountDetailModuleProtocols.h"
#import "AccountDetailModulePresenter.h"

@interface AccountDetailModuleViewController : UIViewController <AccountDetailModuleViewProtocol,
                                                        UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *accountNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountWebsiteLabel;

@property (nonatomic, strong) AccountDetailModulePresenter *presenter;

@end
