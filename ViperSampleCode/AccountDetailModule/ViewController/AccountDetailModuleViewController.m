//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModuleViewController.h"
#import "AccountDetaiModuleHeaderView.h"

@implementation AccountDetailModuleViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Account detail";
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.presenter updateView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) updateAccountInfor
{
    _accountNameLabel.text = [self.presenter.viewModel accountName];
    _accountAddressLabel.text = [self.presenter.viewModel accountAddress];
    _accountPhoneLabel.text = [self.presenter.viewModel accountPhoneNumber];
    _accountWebsiteLabel.text = [self.presenter.viewModel accountWebsite];
}

#pragma mark AccountDetailModuleViewProtocol
-(void) reloadContactList
{
    [self.tableView reloadData];
}

#pragma mark UITableViewDataSource

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *identifier = @"AccountDetaiModuleHeaderView";
    AccountDetaiModuleHeaderView *headerView = [tableView
                                                dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (!headerView) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:identifier
                                                    owner:self options:nil] firstObject];
    }
    __weak id wself = self;
    [headerView setDidTapAddNewContact:^(id sender) {
        [[wself presenter] addContact];
    }];
    headerView.contactCountLabel.text = [self.presenter.viewModel titleForHeaderView];
    headerView.contentView.backgroundColor = [UIColor lightGrayColor];
    return headerView;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.presenter.viewModel numberOfRowInSection:section];
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ContactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    cell.textLabel.text = [self.presenter.viewModel titleForItemAtIndexPath:indexPath];
    cell.detailTextLabel.text = [self.presenter.viewModel subTitleForItemAtIndexPath:indexPath];
    return cell;
}

#pragma mark UITableViewDelegate
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.presenter showContactAtIndexPath:indexPath];
}

-(void) dealloc
{
    NSLog(@"AccountDetailModuleViewController dealloc");
}

@end