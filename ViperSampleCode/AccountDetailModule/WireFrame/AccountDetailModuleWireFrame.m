//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountDetailModuleWireFrame.h"
#import <PureLayout.h>

@interface AccountDetailModuleWireFrame()
@end

@implementation AccountDetailModuleWireFrame

-(instancetype) init
{
    self = [super init];
    return self;
}

- (void)presentAccount:(AccountModel *) account from:(UIViewController*)fromViewController
{
    // Connecting
    
    // Generating module components
    AccountDetailModuleViewController *viewController =
    [self getAccountDetailModuleViewControllerForModule];
    self.presenter.viewModel.accountModel = account;
    //TOODO - New view controller presentation (present, push, pop, .. )
    [fromViewController.navigationController pushViewController:viewController animated:YES];
}

- (void)presentAccount:(AccountModel *) account
                  from:(UIViewController*)fromView
       inContainerView:(UIView *) containerView
{
    // Generating module components
    AccountDetailModuleViewController *viewController =
    [self getAccountDetailModuleViewControllerForModule];
    self.presenter.viewModel.accountModel = account;
    [self.presenter updateView];
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    [fromView addChildViewController:viewController];
    [containerView addSubview:viewController.view];
    [viewController.view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    [viewController didMoveToParentViewController:fromView];
}


#pragma mark AccountDetailModuleWireFrameInputProtocol
-(void) presentAddContactViewForAccount:(id) account
{
    AddEditContactModuleWireFrame *addContactWireFrame = [AddEditContactModuleWireFrame new];
    [addContactWireFrame presentAddEditContactModuleFrom:(UIViewController*)self.presenter.view
                                              forAccount:account];
}

-(void) presentContactDetail:(id) contact
{
    
}

#pragma mark Private methods

-(void) sendAccountModel:(AccountModel *) account
             toPresenter:(AddEditContactModulePresenter *) presenter
{
    [presenter.viewModel setAccountModel:account];
}

-(AccountDetailModuleViewController *) getAccountDetailModuleViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    if (self.presenter) {
        return (AccountDetailModuleViewController *)self.presenter.view;
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        AccountDetailModuleViewController *viewcontroller =
        [storyboard instantiateViewControllerWithIdentifier:@"AccountDetailModuleViewController"];
        
        AccountDetailModulePresenter *presenter = [AccountDetailModulePresenter new];
        id <AccountDetailModuleInteractorInputProtocol,
        AccountDetailModuleDataManagerOutputProtocol,
        AccountDetailModuleServiceManagerOutputProtocol> interactor =
        [AccountDetailModuleInteractor new];
        id <AccountDetailModuleDataManagerInputProtocol> dataManager =
        [AccountDetailModuleDataManager new];
        id <AccountDetailModuleServiceManagerInputProtocol> serviceManager =
        [AccountDetailModuleServiceManager new];
        
        AccountDetailModuleViewModel *viewModel = [AccountDetailModuleViewModel new];
        
        presenter.wireFrame = self;
        presenter.viewModel = viewModel;
        presenter.interactor = interactor;
        interactor.presenter = presenter;
        interactor.dataManager = dataManager;
        dataManager.interactor = interactor;
        interactor.serviceManager = serviceManager;
        serviceManager.interactor = interactor;
        
        viewcontroller.presenter = presenter;
        presenter.view = viewcontroller;
        self.presenter = presenter;
        return viewcontroller;
    }
}

-(void) dealloc
{
    NSLog(@"AccountDetailModuleWireFrame dealloc");
}
@end
