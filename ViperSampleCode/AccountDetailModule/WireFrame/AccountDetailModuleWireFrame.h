//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDetailModuleProtocols.h"
#import "AccountDetailModuleViewController.h"
#import "AccountDetailModuleDataManager.h"
#import "AccountDetailModuleServiceManager.h"
#import "AccountDetailModuleInteractor.h"
#import "AccountDetailModulePresenter.h"
#import "AccountDetailModuleWireframe.h"
#import <UIKit/UIKit.h>

#import "AddEditContactModuleWireFrame.h"
#import "AccountContainerWireFrame.h"

@interface AccountDetailModuleWireFrame : NSObject <AccountDetailModuleWireFrameInputProtocol>

@property (nonatomic, weak) AccountDetailModulePresenter *presenter;
@property (nonatomic, weak) AccountContainerWireFrame *accountContainerWireFrame;

-(AccountDetailModuleViewController *) getAccountDetailModuleViewControllerForModule;

- (void)presentAccount:(AccountModel *) account from:(UIViewController*)fromViewController;
- (void)presentAccount:(AccountModel *) account
                  from:(UIViewController*)fromViewController
       inContainerView:(UIView *) view;
@end
