//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AccountDetailModuleDataManagerOutputProtocol;
@protocol AccountDetailModuleServiceManagerOutputProtocol;
@protocol AccountDetailModuleInteractorOutputProtocol;
@protocol AccountDetailModuleInteractorInputProtocol;
@protocol AccountDetailModuleViewProtocol;
@protocol AccountDetailModulePresenterProtocol;
@protocol AccountDetailModuleDataManagerInputProtocol;
@protocol AccountDetailModuleServiceManagerInputProtocol;
@protocol AccountDetailModuleWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark AccountDetailModuleViewProtocol
@protocol AccountDetailModuleViewProtocol
@required
@property (nonatomic, strong) id <AccountDetailModulePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
-(void) reloadContactList;
-(void) updateAccountInfor;
@end

#pragma mark AccountDetailModulePresenterProtocol
@protocol AccountDetailModulePresenterProtocol
@required
@property (nonatomic, weak) id <AccountDetailModuleViewProtocol> view;
@property (nonatomic, strong) id <AccountDetailModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <AccountDetailModuleWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
-(void) updateView;
-(void) addContact;
-(void) showContactAtIndexPath:(NSIndexPath *) indexPath;
@end

#pragma mark AccountDetailModuleWireFrameInputProtocol
@protocol AccountDetailModuleWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */
-(void) presentAddContactViewForAccount:(id) account;
-(void) presentContactDetail:(id) contact;
@end

#pragma mark AccountDetailModuleInteractorOutputProtocol
@protocol AccountDetailModuleInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
-(void) foundContacts:(NSArray *) contacts;
@end

#pragma mark AccountDetailModuleInteractorInputProtocol
@protocol AccountDetailModuleInteractorInputProtocol
@required
@property (nonatomic, weak) id <AccountDetailModuleInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
-(void) findAllContactByAccount:(id) account;
@end

#pragma mark AccountDetailModuleDataManagerInputProtocol
@protocol AccountDetailModuleDataManagerInputProtocol
@property (nonatomic, weak) id <AccountDetailModuleDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
-(void) fetchAllContactByAccount:(id) account;
@end

#pragma mark AccountDetailModuleDataManagerOutputProtocol
@protocol AccountDetailModuleDataManagerOutputProtocol
@property (nonatomic, strong) id <AccountDetailModuleDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
-(void) fetchedContacts:(NSArray *) contactModels;
@end

#pragma mark AccountDetailModuleServiceManagerInputProtocol
@protocol AccountDetailModuleServiceManagerInputProtocol
@property (nonatomic, weak) id <AccountDetailModuleServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AccountDetailModuleServiceManagerOutputProtocol
@protocol AccountDetailModuleServiceManagerOutputProtocol
@property (nonatomic, strong) id <AccountDetailModuleServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
