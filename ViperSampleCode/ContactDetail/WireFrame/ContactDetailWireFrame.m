//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "ContactDetailWireFrame.h"

@interface ContactDetailWireFrame()
@end

@implementation ContactDetailWireFrame

- (void)presentContactDetailFrom:(UIViewController*)fromViewController
{
    // Generating module components
    ContactDetailViewController *viewController = [self getContactDetailViewControllerForModule];
    ContactDetailPresenter *presenter = [ContactDetailPresenter new];
    id <ContactDetailInteractorInputProtocol,
        ContactDetailDataManagerOutputProtocol,
        ContactDetailServiceManagerOutputProtocol> interactor = [ContactDetailInteractor new];
    id <ContactDetailDataManagerInputProtocol> dataManager = [ContactDetailDataManager new];
    id <ContactDetailServiceManagerInputProtocol> serviceManager = [ContactDetailServiceManager new];
    ContactDetailViewModel *viewModel = [ContactDetailViewModel new];
    
    // Connecting
    presenter.wireFrame = self;
    presenter.view = viewController;
    presenter.viewModel = viewModel;
    presenter.interactor = interactor;
    
    viewController.presenter = presenter;
    
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    interactor.serviceManager = serviceManager;
    
    dataManager.interactor = interactor;
    serviceManager.interactor = interactor;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    //[fromViewController.navigationController pushViewController:viewController];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(ContactDetailViewController *) getContactDetailViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    return nil;
}
@end
