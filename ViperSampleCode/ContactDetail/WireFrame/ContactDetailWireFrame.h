//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailProtocols.h"
#import "ContactDetailViewController.h"
#import "ContactDetailDataManager.h"
#import "ContactDetailServiceManager.h"
#import "ContactDetailInteractor.h"
#import "ContactDetailPresenter.h"
#import "ContactDetailWireframe.h"
#import <UIKit/UIKit.h>

@interface ContactDetailWireFrame : NSObject <ContactDetailWireFrameInputProtocol>

@property (nonatomic, weak) ContactDetailPresenter *presenter;
@property (nonatomic, weak) ContactDetailViewController *viewController;

- (void)presentContactDetailFrom:(id)fromView;

@end
