//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDetailProtocols.h"
#import "ContactDetailPresenter.h"

@interface ContactDetailViewController : UIViewController <ContactDetailViewProtocol>

@property (nonatomic, strong) ContactDetailPresenter *presenter;

@end
