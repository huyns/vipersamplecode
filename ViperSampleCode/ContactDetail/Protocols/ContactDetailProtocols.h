//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ContactDetailDataManagerOutputProtocol;
@protocol ContactDetailServiceManagerOutputProtocol;
@protocol ContactDetailInteractorOutputProtocol;
@protocol ContactDetailInteractorInputProtocol;
@protocol ContactDetailViewProtocol;
@protocol ContactDetailPresenterProtocol;
@protocol ContactDetailDataManagerInputProtocol;
@protocol ContactDetailServiceManagerInputProtocol;
@protocol ContactDetailWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark ContactDetailViewProtocol
@protocol ContactDetailViewProtocol
@required
@property (nonatomic, strong) id <ContactDetailPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark ContactDetailPresenterProtocol
@protocol ContactDetailPresenterProtocol
@required
@property (nonatomic, weak) id <ContactDetailViewProtocol> view;
@property (nonatomic, strong) id <ContactDetailInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ContactDetailWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
@end

#pragma mark ContactDetailWireFrameInputProtocol
@protocol ContactDetailWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */

@end

#pragma mark ContactDetailInteractorInputProtocol
@protocol ContactDetailInteractorInputProtocol
@required
@property (nonatomic, weak) id <ContactDetailInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end

#pragma mark ContactDetailInteractorOutputProtocol
@protocol ContactDetailInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

#pragma mark ContactDetailDataManagerInputProtocol
@protocol ContactDetailDataManagerInputProtocol
@property (nonatomic, weak) id <ContactDetailDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark ContactDetailDataManagerOutputProtocol
@protocol ContactDetailDataManagerOutputProtocol
@property (nonatomic, strong) id <ContactDetailDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
@end

#pragma mark ContactDetailServiceManagerInputProtocol
@protocol ContactDetailServiceManagerInputProtocol
@property (nonatomic, weak) id <ContactDetailServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark ContactDetailServiceManagerOutputProtocol
@protocol ContactDetailServiceManagerOutputProtocol
@property (nonatomic, strong) id <ContactDetailServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
