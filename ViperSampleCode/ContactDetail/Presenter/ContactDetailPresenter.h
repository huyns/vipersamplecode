//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailProtocols.h"
#import "ContactDetailViewModel.h"

@class ContactDetailWireFrame;

@interface ContactDetailPresenter : NSObject <ContactDetailPresenterProtocol, ContactDetailInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <ContactDetailViewProtocol> view;
@property (nonatomic, strong) ContactDetailViewModel * viewModel;
@property (nonatomic, strong) id <ContactDetailInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ContactDetailWireFrameInputProtocol> wireFrame;

@end
