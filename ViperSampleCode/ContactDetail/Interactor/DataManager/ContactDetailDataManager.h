//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailProtocols.h"


@interface ContactDetailDataManager : NSObject <ContactDetailDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <ContactDetailDataManagerOutputProtocol> interactor;

@end