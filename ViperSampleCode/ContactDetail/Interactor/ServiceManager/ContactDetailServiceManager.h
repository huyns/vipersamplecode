//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailProtocols.h"


@interface ContactDetailServiceManager : NSObject <ContactDetailServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <ContactDetailServiceManagerOutputProtocol> interactor;

@end