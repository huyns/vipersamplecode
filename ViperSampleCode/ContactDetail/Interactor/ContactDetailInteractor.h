//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailProtocols.h"


@interface ContactDetailInteractor : NSObject <ContactDetailInteractorInputProtocol, ContactDetailDataManagerOutputProtocol, ContactDetailServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <ContactDetailInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <ContactDetailDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <ContactDetailServiceManagerInputProtocol> serviceManager;

@end