//
//  AppDependencies.m
//  ViperSampleCode
//
//  Created by GEM on 8/24/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AppDependencies.h"
#import "AccountListWireFrame.h"
#import "AccountContainerWireFrame.h"

@interface AppDependencies()

@property (nonatomic, strong) AccountListWireFrame *accountsWireFrame;
@property (nonatomic, strong) AccountContainerWireFrame *accountContainerWireFrame;
@end

@implementation AppDependencies

-(instancetype) init
{
    self = [super init];
    self.accountsWireFrame = [[AccountListWireFrame alloc] init];
    
    return self;
}

- (void)installRootViewControllerIntoWindow:(UIWindow *)window
{
    [self.accountsWireFrame presentAccountListFrom:window];
}

-(void) installRootViewControllerIpad:(UIWindow *) window
{
    self.accountContainerWireFrame = [AccountContainerWireFrame new];
    [self.accountContainerWireFrame presentAccountContainerFrom:window.rootViewController];
}
@end
