//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditAccountModuleProtocols.h"


@interface AddEditAccountModuleDataManager : NSObject <AddEditAccountModuleDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditAccountModuleDataManagerOutputProtocol> interactor;

@end