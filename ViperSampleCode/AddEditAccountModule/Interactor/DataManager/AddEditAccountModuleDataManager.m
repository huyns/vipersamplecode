//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModuleDataManager.h"
#import "Account.h"
#import <MagicalRecord/MagicalRecord.h>
#import "AccountModel.h"

@implementation AddEditAccountModuleDataManager

#pragma mark AddEditAccountModuleDataManagerInputProtocol
-(void) saveAccount:(AccountModel*)account
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    Account *saveAccount = [Account MR_findFirstByAttribute:@"name"
                                                  withValue:account.name
                                                  inContext:context];
    /**
     *  account with this name is already exist
     */
    if (saveAccount) {
        NSString *errorMsg = @"Account with this name already exist, please choose an other name";
        NSError *error = [NSError errorWithDomain:@""
                                             code:0
                                         userInfo:@{NSLocalizedDescriptionKey : errorMsg}];
        [self.interactor didSaveAccount:error];
    }else{
        saveAccount = [Account MR_createEntityInContext:context];
        [saveAccount setName:account.name];
        [saveAccount setPhone:account.phone];
        [saveAccount setAddress:account.address];
        [saveAccount setWebsite:account.website];
        
        [context
         MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
            if (contextDidSave) {
                [self.interactor didSaveAccount:error];
            }
        }];
    }
}
@end