//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModuleInteractor.h"

@implementation AddEditAccountModuleInteractor

#pragma mark AddEditAccountModuleInteractorInputProtocol
-(void) saveAccount:(id) account
{
    [self.dataManager saveAccount:account];
}

#pragma mark AddEditAccountModuleDataManagerOutputProtocol
-(void) didSaveAccount:(NSError *)error
{
    [self.presenter didSaveAccount:error];
}
@end