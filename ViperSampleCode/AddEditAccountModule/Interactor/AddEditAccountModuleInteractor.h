//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditAccountModuleProtocols.h"


@interface AddEditAccountModuleInteractor : NSObject <AddEditAccountModuleInteractorInputProtocol, AddEditAccountModuleDataManagerOutputProtocol, AddEditAccountModuleServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditAccountModuleInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <AddEditAccountModuleDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <AddEditAccountModuleServiceManagerInputProtocol> serviceManager;

@end