//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditAccountModuleProtocols.h"
#import "AddEditAccountModulePresenter.h"

@interface AddEditAccountModuleViewController : UIViewController <AddEditAccountModuleViewProtocol>

@property (nonatomic, strong) AddEditAccountModulePresenter *presenter;


@property (weak, nonatomic) IBOutlet UITextField *accountNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *websiteTextField;
@end
