//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModuleViewController.h"

@implementation AddEditAccountModuleViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) setupNavigation
{
    self.navigationItem.title = @"Add Account";
    UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                      target:self
                                      action:@selector(onSave)];;
    self.navigationItem.rightBarButtonItem = saveBarButton;
    UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                        target:self
                                        action:@selector(onCancel)];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
}

-(void) onSave
{
    [self.presenter.viewModel updateWithName:_accountNameTextField.text
                                     address:_addressTextField.text
                                       phone:_phoneNumberTextField.text
                                     website:_websiteTextField.text];
    [self.presenter saveAccount];
}

-(void) onCancel
{
    [self.presenter cancelAddAccount];
}

-(void) dealloc
{
    NSLog(@"AddEditAccountModuleViewController dealloc");
}
@end