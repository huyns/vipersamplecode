//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModuleWireFrame.h"

@interface AddEditAccountModuleWireFrame()

@end

@implementation AddEditAccountModuleWireFrame

-(instancetype) init
{
    self = [super init];
    return self;
}

- (void)presentAddEditAccountModuleFrom:(UIViewController*)fromViewController
{
    // Generating module components
    AddEditAccountModuleViewController *viewController =
    [self getAddEditAccountModuleViewControllerForModule];
    AddEditAccountModulePresenter *presenter = [AddEditAccountModulePresenter new];
    id <AddEditAccountModuleInteractorInputProtocol,
        AddEditAccountModuleDataManagerOutputProtocol,
        AddEditAccountModuleServiceManagerOutputProtocol> interactor =
    [AddEditAccountModuleInteractor new];
    id <AddEditAccountModuleDataManagerInputProtocol> dataManager =
    [AddEditAccountModuleDataManager new];
    id <AddEditAccountModuleServiceManagerInputProtocol> serviceManager =
    [AddEditAccountModuleServiceManager new];
    
    AddEditAccountModuleViewModel *viewModel = [AddEditAccountModuleViewModel new];
    
    presenter.wireFrame = self;
    presenter.viewModel = viewModel;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    dataManager.interactor = interactor;
    interactor.serviceManager = serviceManager;
    serviceManager.interactor = interactor;
    
    // Connecting
    viewController.presenter = presenter;
    presenter.view = viewController;

    //TOODO - New view controller presentation (present, push, pop, .. )
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:viewController];
    [fromViewController presentViewController:nav animated:YES completion:nil];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(AddEditAccountModuleViewController *) getAddEditAccountModuleViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    AddEditAccountModuleViewController *viewcontroller =
    [storyboard instantiateViewControllerWithIdentifier:@"AddEditAccountModuleViewController"];
    return viewcontroller;
}

#pragma mark AddEditAccountModuleWireFrameInputProtocol
-(void) cancelAddAccount
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

-(void) saveAccount:(NSError *) error
{
    if (error) {
        UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:@"Error"
                                        message:[error localizedDescription]
                                        preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
        [alert addAction:okAction];
        [self.viewController presentViewController:alert animated:YES completion:nil];
    }else{
        /**
         *  notice list view to reload here
         */        
        [self.viewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void) showValidationMessage:(NSString *) errorMsg
{
    if (errorMsg) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Error"
                                    message:errorMsg
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
        [alert addAction:okAction];
        [self.viewController presentViewController:alert animated:YES completion:nil];
    }
}

-(void) dealloc
{
    NSLog(@"AddEditAccountModuleWireFrame dealloc");
}
@end
