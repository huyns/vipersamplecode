//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditAccountModuleProtocols.h"
#import "AddEditAccountModuleViewController.h"
#import "AddEditAccountModuleDataManager.h"
#import "AddEditAccountModuleServiceManager.h"
#import "AddEditAccountModuleInteractor.h"
#import "AddEditAccountModulePresenter.h"
#import "AddEditAccountModuleWireframe.h"
#import <UIKit/UIKit.h>

@interface AddEditAccountModuleWireFrame : NSObject <AddEditAccountModuleWireFrameInputProtocol>

@property (nonatomic, weak) AddEditAccountModulePresenter *presenter;
@property (nonatomic, weak) AddEditAccountModuleViewController *viewController;

- (void)presentAddEditAccountModuleFrom:(id)fromView;

@end
