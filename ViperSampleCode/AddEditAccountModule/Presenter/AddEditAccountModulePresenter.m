//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModulePresenter.h"
#import "AddEditAccountModuleWireframe.h"

@implementation AddEditAccountModulePresenter

#pragma mark AddEditAccountModulePresenterProtocol
-(void) cancelAddAccount
{
    [self.wireFrame cancelAddAccount];
}

-(void) saveAccount
{
    NSString *errorMsg = [self.viewModel validate];
    if (!errorMsg) {
        [self.interactor saveAccount:[self.viewModel getNewAccount]];
    }else{
        [self.wireFrame showValidationMessage:errorMsg];
    }
}

#pragma mark AddEditAccountModuleInteractorOutputProtocol
-(void) didSaveAccount:(NSError *)error
{
    [self.wireFrame saveAccount:error];
}

-(void) dealloc
{
    NSLog(@"AddEditAccountModulePresenter dealloc");
}
@end