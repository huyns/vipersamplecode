//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditAccountModuleProtocols.h"
#import "AddEditAccountModuleViewModel.h"

@class AddEditAccountModuleWireFrame;

@interface AddEditAccountModulePresenter : NSObject <AddEditAccountModulePresenterProtocol, AddEditAccountModuleInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditAccountModuleViewProtocol> view;
@property (nonatomic, strong) AddEditAccountModuleViewModel * viewModel;
@property (nonatomic, strong) id <AddEditAccountModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id<AddEditAccountModuleWireFrameInputProtocol> wireFrame;

@end
