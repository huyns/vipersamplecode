//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AddEditAccountModuleDataManagerOutputProtocol;
@protocol AddEditAccountModuleServiceManagerOutputProtocol;
@protocol AddEditAccountModuleInteractorOutputProtocol;
@protocol AddEditAccountModuleInteractorInputProtocol;
@protocol AddEditAccountModuleViewProtocol;
@protocol AddEditAccountModulePresenterProtocol;
@protocol AddEditAccountModuleDataManagerInputProtocol;
@protocol AddEditAccountModuleServiceManagerInputProtocol;
@protocol AddEditAccountModuleWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark AddEditAccountModuleViewProtocol
@protocol AddEditAccountModuleViewProtocol
@required
@property (nonatomic, strong) id <AddEditAccountModulePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark AddEditAccountModulePresenterProtocol
@protocol AddEditAccountModulePresenterProtocol
@required
@property (nonatomic, weak) id <AddEditAccountModuleViewProtocol> view;
@property (nonatomic, strong) id <AddEditAccountModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id<AddEditAccountModuleWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
-(void) cancelAddAccount;
-(void) saveAccount;
@end

#pragma mark AddEditAccountModuleWireFrameInputProtocol
@protocol AddEditAccountModuleWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */
-(void) cancelAddAccount;
-(void) saveAccount:(NSError *) error;
-(void) showValidationMessage:(NSString *) errorMsg;
@end

#pragma mark AddEditAccountModuleInteractorOutputProtocol
@protocol AddEditAccountModuleInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
-(void) didSaveAccount:(NSError *) error;
@end

#pragma mark AddEditAccountModuleInteractorInputProtocol
@protocol AddEditAccountModuleInteractorInputProtocol
@required
@property (nonatomic, weak) id <AddEditAccountModuleInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
-(void) saveAccount:(id) account;
@end

#pragma mark AddEditAccountModuleDataManagerInputProtocol
@protocol AddEditAccountModuleDataManagerInputProtocol
@property (nonatomic, weak) id <AddEditAccountModuleDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
-(void) saveAccount:(id) account;
@end

#pragma mark AddEditAccountModuleDataManagerOutputProtocol
@protocol AddEditAccountModuleDataManagerOutputProtocol
@property (nonatomic, strong) id <AddEditAccountModuleDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
-(void) didSaveAccount:(NSError *) error;
@end

#pragma mark AddEditAccountModuleServiceManagerInputProtocol
@protocol AddEditAccountModuleServiceManagerInputProtocol
@property (nonatomic, weak) id <AddEditAccountModuleServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AddEditAccountModuleServiceManagerOutputProtocol
@protocol AddEditAccountModuleServiceManagerOutputProtocol
@property (nonatomic, strong) id <AddEditAccountModuleServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
