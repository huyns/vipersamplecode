//
//  AddEditAccountModuleViewModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditAccountModuleViewModel.h"

@implementation AddEditAccountModuleViewModel

-(void) updateWithName:(NSString *) name
               address:(NSString *) address
                 phone:(NSString *) phone
               website:(NSString *) website
{
    _name = name;
    _address = address;
    _phone = phone;
    _website = website;
}

-(AccountModel *) getNewAccount
{
    AccountModel *accountModel = [[AccountModel alloc] init];
    accountModel.name = _name;
    accountModel.address = _address;
    accountModel.phone = _phone;
    accountModel.website = _website;
    
    return accountModel;
}

/**
 *  view model validate itself
 *
 *  @return error string if has
 */
-(NSString *) validate
{
    if (_name.length == 0) {
        return @"Name cannot be empty";
    }
    if (_address.length == 0) {
        return @"Address cannot be empty";
    }
    if (_phone.length == 0) {
        return @"Phone number cannot be empty";
    }
    return nil;
}
@end
