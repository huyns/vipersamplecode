//
//  AddEditAccountModuleViewModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountModel.h"

@interface AddEditAccountModuleViewModel : NSObject

@property (nonatomic, strong) AccountModel *accountModel;
@property (nonatomic, strong) NSString *name, *phone, *address, *website;

-(void) updateWithName:(NSString *) name
               address:(NSString *) address
                 phone:(NSString *) phone
               website:(NSString *) website;
-(AccountModel *) getNewAccount;
-(NSString *) validate;
@end
