//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditContactModuleProtocols.h"
#import "AddEditContactModuleViewModel.h"

@class AddEditContactModuleWireFrame;

@interface AddEditContactModulePresenter : NSObject <AddEditContactModulePresenterProtocol, AddEditContactModuleInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditContactModuleViewProtocol> view;
@property (nonatomic, strong) AddEditContactModuleViewModel * viewModel;
@property (nonatomic, strong) id <AddEditContactModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id<AddEditContactModuleWireFrameInputProtocol> wireFrame;

@end
