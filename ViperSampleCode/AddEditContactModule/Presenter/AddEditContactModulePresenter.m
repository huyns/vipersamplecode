//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModulePresenter.h"
#import "AddEditContactModuleWireframe.h"

@implementation AddEditContactModulePresenter

#pragma mark AddEditContactModulePresenterProtocol
-(void) cancelAddContact
{
    [self.wireFrame cancelAddContact];
}

-(void) saveContact
{
    NSString *validateMsg = [self.viewModel validate];
    if (validateMsg) {
        [self.wireFrame showValidationMessage:validateMsg];
    }else{
        [self.interactor saveContact:[self.viewModel getNewContactModel]
                          forAccount:self.viewModel.accountModel];
    }
}

#pragma mark AddEditContactModuleInteractorOutputProtocol
-(void) didSaveContact:(NSError *) error
{
    [self.wireFrame saveContact:error];
}

-(void) dealloc
{
    NSLog(@"AddEditContactModulePresenter dealloc");
}

@end