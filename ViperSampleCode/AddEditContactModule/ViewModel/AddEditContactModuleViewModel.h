//
//  AddEditContactModuleViewModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactModel.h"
#import "AccountModel.h"

@interface AddEditContactModuleViewModel : NSObject

@property (nonatomic, strong) AccountModel *accountModel;
@property (nonatomic, strong) ContactModel *contactModel;

@property (nonatomic, strong) NSString* dob;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* phone;

-(void) updateWithFirstName:(NSString *) firstName
                   lastName:(NSString *) lastName
                        dob:(NSString *) dob
                phoneNumber:(NSString *) phoneNumber
                      email:(NSString *) email;
-(ContactModel *) getNewContactModel;
-(NSString *) validate;

@end
