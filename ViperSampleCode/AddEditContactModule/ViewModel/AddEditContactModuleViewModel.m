//
//  AddEditContactModuleViewModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModuleViewModel.h"

@implementation AddEditContactModuleViewModel

-(void) updateWithFirstName:(NSString *) firstName
                   lastName:(NSString *) lastName
                        dob:(NSString *) dob
                phoneNumber:(NSString *) phoneNumber
                      email:(NSString *) email
{
    _firstName = firstName;
    _lastName = lastName;
    _dob = dob;
    _phone = phoneNumber;
    _email = email;
}

-(ContactModel *) getNewContactModel
{
    ContactModel *contactModel = [[ContactModel alloc] init];
    contactModel.firstName = _firstName;
    contactModel.lastName = _lastName;
    contactModel.phone = _phone;
    contactModel.email = _email;
    contactModel.dob = _dob;
    
    return contactModel;
}

/**
 *  view model validate itself
 *
 *  @return error string if has
 */
-(NSString *) validate
{
    if (_firstName.length == 0) {
        return @"Fist name cannot be empty";
    }
    if (_lastName.length == 0) {
        return @"Last name cannot be empty";
    }
    if (_email.length == 0) {
        return @"Email cannot be empty";
    }
    return nil;
}

@end
