//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEditContactModuleProtocols.h"
#import "AddEditContactModulePresenter.h"

@interface AddEditContactModuleViewController : UIViewController <AddEditContactModuleViewProtocol>

@property (nonatomic, strong) AddEditContactModulePresenter *presenter;

@property (weak, nonatomic) IBOutlet UITextField *firstNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *lastNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *dobLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailLabel;

@end
