//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModuleViewController.h"

@implementation AddEditContactModuleViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) setupNavigation
{
    self.navigationItem.title = @"Add Contact";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                              target:self
                                              action:@selector(onSave)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                             target:self
                                             action:@selector(onCancel)];
}

-(void) onSave
{
    NSString *firstName = _firstNameLabel.text;
    NSString *lastName = _lastNameLabel.text;
    NSString *dob = _dobLabel.text;
    NSString *phone = _phoneLabel.text;
    NSString *email = _emailLabel.text;
    
    [self.presenter.viewModel updateWithFirstName:firstName
                                         lastName:lastName
                                              dob:dob
                                      phoneNumber:phone
                                            email:email];
    [self.presenter saveContact];
}

-(void) onCancel
{
    [self.presenter cancelAddContact];
}

-(void) dealloc
{
    NSLog(@"AddEditContactModuleViewController dealloc");
}
@end