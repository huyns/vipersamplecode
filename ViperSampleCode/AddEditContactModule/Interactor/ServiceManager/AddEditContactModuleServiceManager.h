//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditContactModuleProtocols.h"


@interface AddEditContactModuleServiceManager : NSObject <AddEditContactModuleServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditContactModuleServiceManagerOutputProtocol> interactor;

@end