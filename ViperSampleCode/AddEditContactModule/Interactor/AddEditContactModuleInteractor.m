//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModuleInteractor.h"

@implementation AddEditContactModuleInteractor

#pragma mark AddEditContactModuleInteractorInputProtocol
-(void) saveContact:(id) contact forAccount:(id) account
{
    [self.dataManager saveContact:contact forAccount:account];
}

#pragma mark AddEditContactModuleDataManagerOutputProtocol
-(void) didSaveContact:(NSError *) error
{
    [self.presenter didSaveContact:error];
}
@end