//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModuleDataManager.h"
#import "AccountModel.h"
#import "ContactModel.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation AddEditContactModuleDataManager

#pragma mark AddEditContactModuleDataManagerInputProtocol
-(void) saveContact:(ContactModel *)contact forAccount:(AccountModel *)account
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    Contact *existContact = [Contact MR_findFirstByAttribute:@"email"
                                                   withValue:contact.email
                                                   inContext:context];
    if (existContact) {
        NSString *errorMsg = @"Contact with this email is already exist!";
        NSError *error = [NSError errorWithDomain:@""
                                             code:0
                                         userInfo:@{NSLocalizedDescriptionKey : errorMsg}];
        [self.interactor didSaveContact:error];
    }else{
        Account *accountEntity = [Account MR_findFirstByAttribute:@"name"
                                                        withValue:account.name
                                                        inContext:context];
        existContact = [Contact MR_createEntityInContext:context];
        [existContact setFirst_name:contact.firstName];
        [existContact setLast_name:contact.lastName];
        existContact.dob = contact.dob;
        existContact.phone = contact.phone;
        existContact.email = contact.email;
        [existContact setAccount:accountEntity];
        
        [context
         MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
            if (contextDidSave) {
                [self.interactor didSaveContact:error];
            }
        }];
    }
}
@end