//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditContactModuleProtocols.h"


@interface AddEditContactModuleInteractor : NSObject <AddEditContactModuleInteractorInputProtocol, AddEditContactModuleDataManagerOutputProtocol, AddEditContactModuleServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <AddEditContactModuleInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <AddEditContactModuleDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <AddEditContactModuleServiceManagerInputProtocol> serviceManager;

@end