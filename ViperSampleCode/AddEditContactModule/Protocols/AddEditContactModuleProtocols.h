//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AddEditContactModuleDataManagerOutputProtocol;
@protocol AddEditContactModuleServiceManagerOutputProtocol;
@protocol AddEditContactModuleInteractorOutputProtocol;
@protocol AddEditContactModuleInteractorInputProtocol;
@protocol AddEditContactModuleViewProtocol;
@protocol AddEditContactModulePresenterProtocol;
@protocol AddEditContactModuleDataManagerInputProtocol;
@protocol AddEditContactModuleServiceManagerInputProtocol;
@protocol AddEditContactModuleWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark AddEditContactModuleViewProtocol
@protocol AddEditContactModuleViewProtocol
@required
@property (nonatomic, strong) id <AddEditContactModulePresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark AddEditContactModulePresenterProtocol
@protocol AddEditContactModulePresenterProtocol
@required
@property (nonatomic, weak) id <AddEditContactModuleViewProtocol> view;
@property (nonatomic, strong) id <AddEditContactModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id<AddEditContactModuleWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
-(void) cancelAddContact;
-(void) saveContact;
@end

#pragma mark AddEditContactModuleWireFrameInputProtocol
@protocol AddEditContactModuleWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */
-(void) cancelAddContact;
-(void) saveContact:(NSError *) error;
-(void) showValidationMessage:(NSString *) msg;
@end

#pragma mark AddEditContactModuleInteractorOutputProtocol
@protocol AddEditContactModuleInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
-(void) didSaveContact:(NSError *) error;
@end

#pragma mark AddEditContactModuleInteractorInputProtocol
@protocol AddEditContactModuleInteractorInputProtocol
@required
@property (nonatomic, weak) id <AddEditContactModuleInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
-(void) saveContact:(id) contact forAccount:(id) account;
@end

#pragma mark AddEditContactModuleDataManagerInputProtocol
@protocol AddEditContactModuleDataManagerInputProtocol
@property (nonatomic, weak) id <AddEditContactModuleDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
-(void) saveContact:(id) contact forAccount:(id) account;
@end

#pragma mark AddEditContactModuleDataManagerOutputProtocol
@protocol AddEditContactModuleDataManagerOutputProtocol
@property (nonatomic, strong) id <AddEditContactModuleDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
-(void) didSaveContact:(NSError *) error;
@end

#pragma mark AddEditContactModuleServiceManagerInputProtocol
@protocol AddEditContactModuleServiceManagerInputProtocol
@property (nonatomic, weak) id <AddEditContactModuleServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AddEditContactModuleServiceManagerOutputProtocol
@protocol AddEditContactModuleServiceManagerOutputProtocol
@property (nonatomic, strong) id <AddEditContactModuleServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
