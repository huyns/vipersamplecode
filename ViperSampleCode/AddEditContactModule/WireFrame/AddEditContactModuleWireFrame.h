//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddEditContactModuleProtocols.h"
#import "AddEditContactModuleViewController.h"
#import "AddEditContactModuleDataManager.h"
#import "AddEditContactModuleServiceManager.h"
#import "AddEditContactModuleInteractor.h"
#import "AddEditContactModulePresenter.h"
#import "AddEditContactModuleWireframe.h"
#import <UIKit/UIKit.h>

@interface AddEditContactModuleWireFrame : NSObject <AddEditContactModuleWireFrameInputProtocol>

@property (nonatomic, weak) AddEditContactModulePresenter *presenter;
@property (nonatomic, weak) AddEditContactModuleViewController *viewController;
- (void)presentAddEditContactModuleFrom:(UIViewController*)fromViewController
                             forAccount:(AccountModel *) account;
@end
