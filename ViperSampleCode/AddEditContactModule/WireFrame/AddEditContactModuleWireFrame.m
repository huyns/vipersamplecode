//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AddEditContactModuleWireFrame.h"

@interface AddEditContactModuleWireFrame()

@end

@implementation AddEditContactModuleWireFrame

-(instancetype) init
{
    self = [super init];
    return self;
}

- (void)presentAddEditContactModuleFrom:(UIViewController*)fromViewController
                             forAccount:(AccountModel *) account
{
    // Generating module components
    AddEditContactModuleViewController *viewController =
    [self getAddEditContactModuleViewControllerForModule];
    AddEditContactModulePresenter *presenter = [AddEditContactModulePresenter new];
    id <AddEditContactModuleInteractorInputProtocol,
        AddEditContactModuleDataManagerOutputProtocol,
        AddEditContactModuleServiceManagerOutputProtocol> interactor =
    [AddEditContactModuleInteractor new];
    id <AddEditContactModuleDataManagerInputProtocol> dataManager =
    [AddEditContactModuleDataManager new];
    id <AddEditContactModuleServiceManagerInputProtocol> serviceManager =
    [AddEditContactModuleServiceManager new];
    
    AddEditContactModuleViewModel *viewModel = [AddEditContactModuleViewModel new];
    
    presenter.wireFrame = self;
    presenter.view = viewController;
    presenter.viewModel = viewModel;
    presenter.viewModel.accountModel = account;
    presenter.interactor = interactor;
    
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    interactor.serviceManager = serviceManager;
    
    dataManager.interactor = interactor;
    serviceManager.interactor = interactor;
    
    // Connecting
    viewController.presenter = presenter;

    //TOODO - New view controller presentation (present, push, pop, .. )
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:viewController];
    [fromViewController presentViewController:nav animated:YES completion:nil];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(AddEditContactModuleViewController *) getAddEditContactModuleViewControllerForModule
{
    //TODO - init module's viewcontroller here
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    AddEditContactModuleViewController *viewcontroller =
    [storyboard instantiateViewControllerWithIdentifier:@"AddEditContactModuleViewController"];
    
    return viewcontroller;
}

#pragma mark AddEditContactModuleWireFrameInputProtocol
-(void) cancelAddContact
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

-(void) saveContact:(NSError *) error
{
    if (error) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Error"
                                    message:[error localizedDescription]
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
        [alert addAction:okAction];
        [self.viewController presentViewController:alert animated:YES completion:nil];
    }else{
        /**
         *  notice list view to reload here
         */
        [self.viewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void) showValidationMessage:(NSString *) msg
{
    if (msg) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Error"
                                    message:msg
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
        [alert addAction:okAction];
        [self.viewController presentViewController:alert animated:YES completion:nil];
    }
}

-(void) dealloc
{
    NSLog(@"AddEditContactModuleWireFrame dealloc");
}

@end
