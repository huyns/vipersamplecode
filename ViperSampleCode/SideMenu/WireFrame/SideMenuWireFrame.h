//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideMenuProtocols.h"
#import "SideMenuViewController.h"
#import "SideMenuDataManager.h"
#import "SideMenuServiceManager.h"
#import "SideMenuInteractor.h"
#import "SideMenuPresenter.h"
#import "SideMenuWireframe.h"
#import <UIKit/UIKit.h>

@interface SideMenuWireFrame : NSObject <SideMenuWireFrameInputProtocol>

@property (nonatomic, weak) SideMenuPresenter *presenter;
@property (nonatomic, weak) SideMenuViewController *viewController;

- (void)presentSideMenuFrom:(id)fromView;

@end
