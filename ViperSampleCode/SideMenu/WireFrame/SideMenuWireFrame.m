//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "SideMenuWireFrame.h"

@interface SideMenuWireFrame()
@end

@implementation SideMenuWireFrame

- (void)presentSideMenuFrom:(UIViewController*)fromViewController
{
    // Generating module components
    SideMenuViewController *viewController = [self getSideMenuViewControllerForModule];
    SideMenuPresenter *presenter = [SideMenuPresenter new];
    id <SideMenuInteractorInputProtocol,
        SideMenuDataManagerOutputProtocol,
        SideMenuServiceManagerOutputProtocol> interactor = [SideMenuInteractor new];
    id <SideMenuDataManagerInputProtocol> dataManager = [SideMenuDataManager new];
    id <SideMenuServiceManagerInputProtocol> serviceManager = [SideMenuServiceManager new];
    SideMenuViewModel *viewModel = [SideMenuViewModel new];
    
    // Connecting
    presenter.wireFrame = self;
    presenter.view = viewController;
    presenter.viewModel = viewModel;
    presenter.interactor = interactor;
    
    viewController.presenter = presenter;
    
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    interactor.serviceManager = serviceManager;
    
    dataManager.interactor = interactor;
    serviceManager.interactor = interactor;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    //[fromViewController.navigationController pushViewController:viewController];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(SideMenuViewController *) getSideMenuViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    return nil;
}
@end
