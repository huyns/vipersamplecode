//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideMenuProtocols.h"


@interface SideMenuDataManager : NSObject <SideMenuDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <SideMenuDataManagerOutputProtocol> interactor;

@end