//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideMenuProtocols.h"


@interface SideMenuInteractor : NSObject <SideMenuInteractorInputProtocol, SideMenuDataManagerOutputProtocol, SideMenuServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <SideMenuInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <SideMenuDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <SideMenuServiceManagerInputProtocol> serviceManager;

@end