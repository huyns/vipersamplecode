//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SideMenuDataManagerOutputProtocol;
@protocol SideMenuServiceManagerOutputProtocol;
@protocol SideMenuInteractorOutputProtocol;
@protocol SideMenuInteractorInputProtocol;
@protocol SideMenuViewProtocol;
@protocol SideMenuPresenterProtocol;
@protocol SideMenuDataManagerInputProtocol;
@protocol SideMenuServiceManagerInputProtocol;
@protocol SideMenuWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark SideMenuViewProtocol
@protocol SideMenuViewProtocol
@required
@property (nonatomic, strong) id <SideMenuPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark SideMenuPresenterProtocol
@protocol SideMenuPresenterProtocol
@required
@property (nonatomic, weak) id <SideMenuViewProtocol> view;
@property (nonatomic, strong) id <SideMenuInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <SideMenuWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
@end

#pragma mark SideMenuWireFrameInputProtocol
@protocol SideMenuWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */

@end

#pragma mark SideMenuInteractorInputProtocol
@protocol SideMenuInteractorInputProtocol
@required
@property (nonatomic, weak) id <SideMenuInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end

#pragma mark SideMenuInteractorOutputProtocol
@protocol SideMenuInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

#pragma mark SideMenuDataManagerInputProtocol
@protocol SideMenuDataManagerInputProtocol
@property (nonatomic, weak) id <SideMenuDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark SideMenuDataManagerOutputProtocol
@protocol SideMenuDataManagerOutputProtocol
@property (nonatomic, strong) id <SideMenuDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
@end

#pragma mark SideMenuServiceManagerInputProtocol
@protocol SideMenuServiceManagerInputProtocol
@property (nonatomic, weak) id <SideMenuServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark SideMenuServiceManagerOutputProtocol
@protocol SideMenuServiceManagerOutputProtocol
@property (nonatomic, strong) id <SideMenuServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
