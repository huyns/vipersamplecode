//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuProtocols.h"
#import "SideMenuPresenter.h"

@interface SideMenuViewController : UIViewController <SideMenuViewProtocol>

@property (nonatomic, strong) SideMenuPresenter *presenter;

@end
