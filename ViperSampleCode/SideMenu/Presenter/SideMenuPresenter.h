//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideMenuProtocols.h"
#import "SideMenuViewModel.h"

@class SideMenuWireFrame;

@interface SideMenuPresenter : NSObject <SideMenuPresenterProtocol, SideMenuInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <SideMenuViewProtocol> view;
@property (nonatomic, strong) SideMenuViewModel * viewModel;
@property (nonatomic, strong) id <SideMenuInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <SideMenuWireFrameInputProtocol> wireFrame;

@end
