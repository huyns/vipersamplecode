//
//  AppDelegate.h
//  ViperSampleCode
//
//  Created by GEM on 8/23/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

