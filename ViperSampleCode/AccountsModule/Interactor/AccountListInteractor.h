//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountListProtocols.h"

@interface AccountListInteractor : NSObject <AccountListInteractorInputProtocol, AccountListDataManagerOutputProtocol, AccountListServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountListInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <AccountListDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <AccountListServiceManagerInputProtocol> serviceManager;

@end