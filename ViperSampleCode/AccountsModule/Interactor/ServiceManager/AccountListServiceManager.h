//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountListProtocols.h"


@interface AccountListServiceManager : NSObject <AccountListServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountListServiceManagerOutputProtocol> interactor;

@end