//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountListProtocols.h"


@interface AccountListDataManager : NSObject <AccountListDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountListDataManagerOutputProtocol> interactor;

@end