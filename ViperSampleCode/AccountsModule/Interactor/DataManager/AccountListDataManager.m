//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountListDataManager.h"
#import "Account.h"
#import "AccountModel.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation AccountListDataManager

#pragma mark AccountListDataManagerInputProtocol
-(void) fetchAllAccounts
{
    NSArray *accounts = [Account MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [self.interactor didFetchedAccounts:[AccountModel accountObjectsFromAccountEntities:accounts]];
}
@end