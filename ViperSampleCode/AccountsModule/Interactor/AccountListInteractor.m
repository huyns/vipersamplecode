//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountListInteractor.h"

@implementation AccountListInteractor

#pragma mark AccountListInteractorInputProtocol
-(void) findAllAccounts
{
    [self.dataManager fetchAllAccounts];
}

#pragma mark AccountListDataManagerOutputProtocol
-(void) didFetchedAccounts:(NSArray *) array
{
    [self.presenter didFoundAccounts:array];
}

@end