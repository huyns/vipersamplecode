//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountListPresenter.h"

@interface AccountListViewController : UIViewController <AccountListViewProtocol, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) AccountListPresenter *presenter;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
