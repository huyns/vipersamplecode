//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AccountListDataManagerOutputProtocol;
@protocol AccountListServiceManagerOutputProtocol;
@protocol AccountListInteractorOutputProtocol;
@protocol AccountListInteractorInputProtocol;
@protocol AccountListViewProtocol;
@protocol AccountListPresenterProtocol;
@protocol AccountListDataManagerInputProtocol;
@protocol AccountListServiceManagerInputProtocol;
@protocol AccountListWireFrameInputProtocol;


typedef void (^CompletionBlock)(NSError **error);

#pragma mark AccountListViewProtocol
@protocol AccountListViewProtocol
@required
@property (nonatomic, strong) id <AccountListPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
-(void) reloadTableView;
@end

#pragma mark AccountListPresenterProtocol
@protocol AccountListPresenterProtocol
@required
@property (nonatomic, weak) id <AccountListViewProtocol> view;
@property (nonatomic, strong) id <AccountListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <AccountListWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
-(void) updateView;
-(void) addAccount;
-(void) showAccountAtIndexPath:(NSIndexPath *) indexPath;
@end

#pragma mark AccountListWireFrameInputProtocol
@protocol AccountListWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */
-(void) presentAddAccountView;
-(void) presentAccountDetail:(id) account;
@end

#pragma mark AccountListInteractorOutputProtocol
@protocol AccountListInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
-(void) didFoundAccounts:(NSArray *) accounts;
@end

#pragma mark AccountListInteractorInputProtocol
@protocol AccountListInteractorInputProtocol
@required
@property (nonatomic, weak) id <AccountListInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
-(void) findAllAccounts;
@end

#pragma mark AccountListDataManagerInputProtocol
@protocol AccountListDataManagerInputProtocol
@property (nonatomic, weak) id <AccountListDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
-(void) fetchAllAccounts;
@end

#pragma mark AccountListDataManagerOutputProtocol
@protocol AccountListDataManagerOutputProtocol
@property (nonatomic, strong) id <AccountListDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
-(void) didFetchedAccounts:(NSArray *) array;
@end

#pragma mark AccountListServiceManagerInputProtocol
@protocol AccountListServiceManagerInputProtocol
@property (nonatomic, weak) id <AccountListServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AccountListServiceManagerOutputProtocol
@protocol AccountListServiceManagerOutputProtocol
@property (nonatomic, strong) id <AccountListServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
