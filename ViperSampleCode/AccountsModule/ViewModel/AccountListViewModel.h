//
//  AccountListViewModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountListViewModel : NSObject

@property (nonatomic, strong) NSArray *data;

-(NSInteger) numberOfRowInSection:(NSInteger) section;
-(NSString *) titleForItemAtIndexPath:(NSIndexPath *) indexPath;
-(NSString *) subTitleForItemAtIndexPath:(NSIndexPath *) indexPath;
@end
