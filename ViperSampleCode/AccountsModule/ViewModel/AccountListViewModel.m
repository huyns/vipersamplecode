//
//  AccountListViewModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountListViewModel.h"
#import "AccountModel.h"
#import <UIKit/UIKit.h>

@implementation AccountListViewModel

-(NSInteger) numberOfRowInSection:(NSInteger) section
{
    return self.data.count;
}

-(NSString *) titleForItemAtIndexPath:(NSIndexPath *) indexPath
{
    AccountModel *accountModel = [self.data objectAtIndex:indexPath.row];
    return accountModel.name;
}

-(NSString *) subTitleForItemAtIndexPath:(NSIndexPath *) indexPath
{
    AccountModel *accountModel = [self.data objectAtIndex:indexPath.row];
    return accountModel.address;
}

@end
