//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountListProtocols.h"
#import "AccountListViewController.h"
#import "AccountListDataManager.h"
#import "AccountListServiceManager.h"
#import "AccountListInteractor.h"
#import "AccountListPresenter.h"
#import "AccountListWireframe.h"

#import "AccountContainerWireFrame.h"
#import "AddEditAccountModuleWireFrame.h"
#import "AccountDetailModuleWireFrame.h"
#import <UIKit/UIKit.h>

@interface AccountListWireFrame : NSObject <AccountListWireFrameInputProtocol>

@property (nonatomic, weak) AccountListPresenter *presenter;
-(AccountListViewController *) getAccountListViewControllerForModule;
#pragma mark other module's wireframe
@property (nonatomic, weak) AccountContainerWireFrame *accountContainerWireFrame;
@property (nonatomic, strong) AccountDetailModuleWireFrame *accountDetailWireFrame;

#pragma mark public methods
- (void)presentAccountListFrom:(id)fromView;
- (void)presentAccountListFrom:(id)fromView inContainerView:(UIView *) containerView;
@end
