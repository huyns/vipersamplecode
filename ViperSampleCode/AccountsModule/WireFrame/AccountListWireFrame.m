//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountListWireFrame.h"
#import <PureLayout.h>

@interface AccountListWireFrame()
@end

@implementation AccountListWireFrame

-(instancetype) init
{
    self = [super init];
    return self;
}

- (void)presentAccountListFrom:(UIWindow*) window
{
    // Generating module components
    AccountListViewController *viewController = [self getAccountListViewControllerForModule];
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    UINavigationController *rootNav = (UINavigationController*)window.rootViewController;
    [rootNav setViewControllers:@[viewController]];
}

- (void)presentAccountListFrom:(id)fromView inContainerView:(UIView *) containerView
{
    AccountListViewController *viewController = [self getAccountListViewControllerForModule];
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    
    [fromView addChildViewController:viewController];
    [containerView addSubview:viewController.view];
    [viewController.view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    [viewController didMoveToParentViewController:fromView];
}

#pragma mark AccountListWireFrameInputProtocol
-(void) presentAddAccountView
{
    AddEditAccountModuleWireFrame *addAccountWireFrame = [AddEditAccountModuleWireFrame new];
    [addAccountWireFrame presentAddEditAccountModuleFrom:[self getAccountListViewControllerForModule]];
//    self.addAccountWireFrame = addAccountWireFrame;
}

-(void) presentAccountDetail:(AccountModel *)account
{
    if (isPhone) {
        AccountDetailModuleWireFrame *accountDetailWireFrame = [AccountDetailModuleWireFrame new];
        [accountDetailWireFrame presentAccount:account from:[self getAccountListViewControllerForModule]];
//        self.accountDetailWireFrame = accountDetailWireFrame;
    }else{
        [self.accountContainerWireFrame presentAccount:account];
    }
}

#pragma mark private functions

-(AccountListViewController *) getAccountListViewControllerForModule
{
    //TODO - init module's viewcontroller here
    if (self.presenter) {
        return (AccountListViewController*)self.presenter.view;
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        AccountListViewController *viewController = [storyboard
                                                     instantiateViewControllerWithIdentifier:
                                                     @"AccountListViewController"];
        AccountListPresenter *presenter = [AccountListPresenter new];
        id <AccountListInteractorInputProtocol,
        AccountListDataManagerOutputProtocol,
        AccountListServiceManagerOutputProtocol> interactor = [AccountListInteractor new];
        id <AccountListDataManagerInputProtocol> dataManager = [AccountListDataManager new];
        id <AccountListServiceManagerInputProtocol> serviceManager = [AccountListServiceManager new];
        
        AccountListViewModel *viewModel = [AccountListViewModel new];
        
        presenter.wireFrame = self;
        presenter.view = viewController;
        presenter.viewModel = viewModel;
        presenter.interactor = interactor;
        
        interactor.presenter = presenter;
        interactor.dataManager = dataManager;
        interactor.serviceManager = serviceManager;
        
        dataManager.interactor = interactor;
        serviceManager.interactor = interactor;
        
        // Connecting
        viewController.presenter = presenter;
        
        self.presenter = presenter;
        return viewController;
    }
}


-(void) sendAccountModel:(AccountModel *) account
             toPresenter:(AccountDetailModulePresenter *) presenter
{
    [presenter.viewModel setAccountModel:account];
}
@end
