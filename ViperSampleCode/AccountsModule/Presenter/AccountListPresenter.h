//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountListProtocols.h"
#import "AccountListViewModel.h"

@class AccountListWireFrame;

@interface AccountListPresenter : NSObject <AccountListPresenterProtocol, AccountListInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountListViewProtocol> view;
@property (nonatomic, strong) AccountListViewModel * viewModel;
@property (nonatomic, strong) id <AccountListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id<AccountListWireFrameInputProtocol> wireFrame;

@end
