//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountListPresenter.h"
#import "AccountListWireframe.h"

@implementation AccountListPresenter

#pragma mark AccountListPresenterProtocol
-(void) updateView
{
    [self.interactor findAllAccounts];
}

-(void) addAccount
{
    [self.wireFrame presentAddAccountView];
}

-(void) showAccountAtIndexPath:(NSIndexPath *) indexPath
{
    [self.wireFrame presentAccountDetail:self.viewModel.data[indexPath.row]];
}

#pragma mark AccountListInteractorOutputProtocol
-(void) didFoundAccounts:(NSArray *)accounts
{
    [self.viewModel setData:accounts];
    [self.view reloadTableView];
}
@end