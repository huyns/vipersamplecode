//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ContactListDataManagerOutputProtocol;
@protocol ContactListServiceManagerOutputProtocol;
@protocol ContactListInteractorOutputProtocol;
@protocol ContactListInteractorInputProtocol;
@protocol ContactListViewProtocol;
@protocol ContactListPresenterProtocol;
@protocol ContactListDataManagerInputProtocol;
@protocol ContactListServiceManagerInputProtocol;
@protocol ContactListWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark ContactListViewProtocol
@protocol ContactListViewProtocol
@required
@property (nonatomic, strong) id <ContactListPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark ContactListPresenterProtocol
@protocol ContactListPresenterProtocol
@required
@property (nonatomic, weak) id <ContactListViewProtocol> view;
@property (nonatomic, strong) id <ContactListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ContactListWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
@end

#pragma mark ContactListWireFrameInputProtocol
@protocol ContactListWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */

@end

#pragma mark ContactListInteractorInputProtocol
@protocol ContactListInteractorInputProtocol
@required
@property (nonatomic, weak) id <ContactListInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end

#pragma mark ContactListInteractorOutputProtocol
@protocol ContactListInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

#pragma mark ContactListDataManagerInputProtocol
@protocol ContactListDataManagerInputProtocol
@property (nonatomic, weak) id <ContactListDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark ContactListDataManagerOutputProtocol
@protocol ContactListDataManagerOutputProtocol
@property (nonatomic, strong) id <ContactListDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
@end

#pragma mark ContactListServiceManagerInputProtocol
@protocol ContactListServiceManagerInputProtocol
@property (nonatomic, weak) id <ContactListServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark ContactListServiceManagerOutputProtocol
@protocol ContactListServiceManagerOutputProtocol
@property (nonatomic, strong) id <ContactListServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
