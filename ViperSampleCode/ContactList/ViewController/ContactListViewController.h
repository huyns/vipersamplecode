//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactListProtocols.h"
#import "ContactListPresenter.h"

@interface ContactListViewController : UIViewController <ContactListViewProtocol>

@property (nonatomic, strong) ContactListPresenter *presenter;

@end
