//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactListProtocols.h"


@interface ContactListServiceManager : NSObject <ContactListServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <ContactListServiceManagerOutputProtocol> interactor;

@end