//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactListProtocols.h"


@interface ContactListInteractor : NSObject <ContactListInteractorInputProtocol, ContactListDataManagerOutputProtocol, ContactListServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <ContactListInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <ContactListDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <ContactListServiceManagerInputProtocol> serviceManager;

@end