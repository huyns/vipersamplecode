//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactListProtocols.h"
#import "ContactListViewModel.h"

@class ContactListWireFrame;

@interface ContactListPresenter : NSObject <ContactListPresenterProtocol, ContactListInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <ContactListViewProtocol> view;
@property (nonatomic, strong) ContactListViewModel * viewModel;
@property (nonatomic, strong) id <ContactListInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ContactListWireFrameInputProtocol> wireFrame;

@end
