//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactListProtocols.h"
#import "ContactListViewController.h"
#import "ContactListDataManager.h"
#import "ContactListServiceManager.h"
#import "ContactListInteractor.h"
#import "ContactListPresenter.h"
#import "ContactListWireframe.h"
#import <UIKit/UIKit.h>

@interface ContactListWireFrame : NSObject <ContactListWireFrameInputProtocol>

@property (nonatomic, weak) ContactListPresenter *presenter;
@property (nonatomic, weak) ContactListViewController *viewController;

- (void)presentContactListFrom:(id)fromView;

@end
