//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "ContactListWireFrame.h"

@interface ContactListWireFrame()
@end

@implementation ContactListWireFrame

- (void)presentContactListFrom:(UIViewController*)fromViewController
{
    // Generating module components
    ContactListViewController *viewController = [self getContactListViewControllerForModule];
    ContactListPresenter *presenter = [ContactListPresenter new];
    id <ContactListInteractorInputProtocol,
        ContactListDataManagerOutputProtocol,
        ContactListServiceManagerOutputProtocol> interactor = [ContactListInteractor new];
    id <ContactListDataManagerInputProtocol> dataManager = [ContactListDataManager new];
    id <ContactListServiceManagerInputProtocol> serviceManager = [ContactListServiceManager new];
    ContactListViewModel *viewModel = [ContactListViewModel new];
    
    // Connecting
    presenter.wireFrame = self;
    presenter.view = viewController;
    presenter.viewModel = viewModel;
    presenter.interactor = interactor;
    
    viewController.presenter = presenter;
    
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    interactor.serviceManager = serviceManager;
    
    dataManager.interactor = interactor;
    serviceManager.interactor = interactor;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    //[fromViewController.navigationController pushViewController:viewController];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(ContactListViewController *) getContactListViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    return nil;
}
@end
