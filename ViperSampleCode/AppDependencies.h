//
//  AppDependencies.h
//  ViperSampleCode
//
//  Created by GEM on 8/24/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppDependencies : NSObject
- (void)installRootViewControllerIntoWindow:(UIWindow *)window;
-(void) installRootViewControllerIpad:(UIWindow *) window;
@end
