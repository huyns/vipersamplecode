//
//  ContactModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface ContactModel : NSObject

@property (nonatomic, strong) NSString* dob;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* phone;

-(instancetype) initWithContactEntity:(Contact *) contact;
+(NSArray *) contactObjectsFromContactEntities:(NSArray *) contacts;
@end
