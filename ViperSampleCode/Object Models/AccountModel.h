//
//  AccountModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account.h"

@interface AccountModel : NSObject

@property (nonatomic, strong) NSString *name, *phone, *address, *website;

-(instancetype) initWithAccountEntity:(Account *) account;
+(NSArray *) accountObjectsFromAccountEntities:(NSArray *) accounts;
@end
