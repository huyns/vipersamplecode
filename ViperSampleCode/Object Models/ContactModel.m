//
//  ContactModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "ContactModel.h"

@implementation ContactModel

-(instancetype) initWithContactEntity:(Contact *) contact
{
    self = [super init];
    
    self.dob = contact.dob;
    self.email = contact.email;
    self.firstName = contact.first_name;
    self.lastName = contact.last_name;
    self.phone = contact.phone;
    
    return self;
}

+(NSArray *) contactObjectsFromContactEntities:(NSArray *) contacts
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:contacts.count];
    ContactModel *contactModel = nil;
    for (Contact *contact in contacts) {
        contactModel = [[ContactModel alloc] initWithContactEntity:contact];
        [array addObject:contactModel];
    }
    
    return array;
}

@end
