//
//  AccountModel.m
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountModel.h"

@implementation AccountModel

-(instancetype) initWithAccountEntity:(Account *) account
{
    self = [super init];
    
    self.name = account.name;
    self.address = account.address;
    self.phone = account.phone;
    self.website = account.website;
    
    return self;
}

+(NSArray *) accountObjectsFromAccountEntities:(NSArray *) accounts
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:accounts.count];
    AccountModel *accountModel = nil;
    for (Account *account in accounts) {
        accountModel = [[AccountModel alloc] initWithAccountEntity:account];
        [array addObject:accountModel];
    }
    
    return array;
}

@end
