//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountContainerProtocols.h"


@interface AccountContainerInteractor : NSObject <AccountContainerInteractorInputProtocol, AccountContainerDataManagerOutputProtocol, AccountContainerServiceManagerOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountContainerInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <AccountContainerDataManagerInputProtocol> dataManager;
@property (nonatomic, strong) id <AccountContainerServiceManagerInputProtocol> serviceManager;

@end