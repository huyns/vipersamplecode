//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountContainerProtocols.h"


@interface AccountContainerServiceManager : NSObject <AccountContainerServiceManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountContainerServiceManagerOutputProtocol> interactor;

@end