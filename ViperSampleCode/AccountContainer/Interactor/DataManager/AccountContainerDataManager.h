//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountContainerProtocols.h"


@interface AccountContainerDataManager : NSObject <AccountContainerDataManagerInputProtocol>

// Properties
@property (nonatomic, weak) id <AccountContainerDataManagerOutputProtocol> interactor;

@end