//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountContainerProtocols.h"
#import "AccountContainerViewController.h"
#import "AccountContainerDataManager.h"
#import "AccountContainerServiceManager.h"
#import "AccountContainerInteractor.h"
#import "AccountContainerPresenter.h"
#import "AccountContainerWireframe.h"
#import <UIKit/UIKit.h>

#import "AccountListWireFrame.h"
#import "AccountDetailModuleWireFrame.h"
@interface AccountContainerWireFrame : NSObject <AccountContainerWireFrameInputProtocol>

@property (nonatomic, weak) AccountContainerPresenter *presenter;
@property (nonatomic, weak) AccountContainerViewController *viewController;

@property (nonatomic, strong) AccountListWireFrame *accountListWireFrame;
@property (nonatomic, strong) AccountDetailModuleWireFrame *accountDetailWireFrame;

- (void)presentAccountContainerFrom:(id)fromView;

@end
