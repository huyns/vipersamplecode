//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountContainerWireFrame.h"
#import "AddEditAccountModuleWireFrame.h"

@interface AccountContainerWireFrame()
@end

@implementation AccountContainerWireFrame

- (void)presentAccountContainerFrom:(UIViewController*)fromViewController
{
    // Generating module components
    AccountContainerViewController *viewController =
    [self getAccountContainerViewControllerForModule];
    AccountContainerPresenter *presenter = [AccountContainerPresenter new];
    id <AccountContainerInteractorInputProtocol,
        AccountContainerDataManagerOutputProtocol,
        AccountContainerServiceManagerOutputProtocol> interactor = [AccountContainerInteractor new];
    id <AccountContainerDataManagerInputProtocol> dataManager = [AccountContainerDataManager new];
    id <AccountContainerServiceManagerInputProtocol> serviceManager =
    [AccountContainerServiceManager new];
    AccountContainerViewModel *viewModel = [AccountContainerViewModel new];
    
    // Connecting
    presenter.wireFrame = self;
    presenter.view = viewController;
    presenter.viewModel = viewModel;
    presenter.interactor = interactor;
    
    viewController.presenter = presenter;
    
    interactor.presenter = presenter;
    interactor.dataManager = dataManager;
    interactor.serviceManager = serviceManager;
    
    dataManager.interactor = interactor;
    serviceManager.interactor = interactor;
    
    //TOODO - New view controller presentation (present, push, pop, .. )
    //[fromViewController.navigationController pushViewController:viewController];
    
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:viewController];
    [fromViewController presentViewController:nav animated:YES completion:nil];
    
    self.viewController = viewController;
    self.presenter = presenter;
}

-(AccountContainerViewController *) getAccountContainerViewControllerForModule
{
    //TODO - init module's viewcontroller here
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_ipad" bundle:nil];
    AccountContainerViewController *viewcontroller = [storyboard
                                                      instantiateViewControllerWithIdentifier:
                                                      @"AccountContainerViewController"];
    return viewcontroller;
}

#pragma mark AccountContainerWireFrameInputProtocol
-(void) presentAccountListInContainerView:(id) view
{
    AccountListWireFrame *accountListWireFrame = [AccountListWireFrame new];
    [accountListWireFrame presentAccountListFrom:self.viewController inContainerView:view];
    accountListWireFrame.accountContainerWireFrame = self;
    self.accountListWireFrame = accountListWireFrame;
}

-(void) presentAccount:(id) account
{
    if (!self.accountDetailWireFrame) {
        self.accountDetailWireFrame = [AccountDetailModuleWireFrame new];
    }
    [self.accountDetailWireFrame presentAccount:account
                                           from:self.viewController
                                inContainerView:self.viewController.accountDetailContainerView];
}

-(void) dismissView
{
    [self.viewController.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) presentAddAccountView
{
    AddEditAccountModuleWireFrame *addAccountWireFrame = [AddEditAccountModuleWireFrame new];
    [addAccountWireFrame presentAddEditAccountModuleFrom:self.viewController];
}
@end
