//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountContainerProtocols.h"
#import "AccountContainerViewModel.h"

@class AccountContainerWireFrame;

@interface AccountContainerPresenter : NSObject <AccountContainerPresenterProtocol, AccountContainerInteractorOutputProtocol>

// Properties
@property (nonatomic, weak) id <AccountContainerViewProtocol> view;
@property (nonatomic, strong) AccountContainerViewModel * viewModel;
@property (nonatomic, strong) id <AccountContainerInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <AccountContainerWireFrameInputProtocol> wireFrame;

@end
