//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountContainerPresenter.h"
#import "AccountContainerWireframe.h"

@implementation AccountContainerPresenter

#pragma mark AccountContainerPresenterProtocol
-(void) showAccountList:(UIView *) containerView
{
    [self.wireFrame presentAccountListInContainerView:containerView];
}

-(void) showAccount:(id) account
{
    [self.wireFrame presentAccount:account];
}

-(void) dismissView
{
    [self.wireFrame dismissView];
}

-(void) addAccount
{
    [self.wireFrame presentAddAccountView];
}
#pragma mark AccountContainerInteractorOutputProtocol

@end