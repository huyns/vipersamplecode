//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "AccountContainerViewController.h"

@implementation AccountContainerViewController

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                        target:self action:@selector(onCancel:)];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
    
    UIBarButtonItem *addAccountBarButton = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                            target:self action:@selector(onAddAccount:)];
    self.navigationItem.rightBarButtonItem = addAccountBarButton;
    
    [self.presenter showAccountList:_accountListContainerView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) onCancel:(id) sender
{
    [self.presenter dismissView];
}

-(void) onAddAccount:(id) sender
{
    [self.presenter addAccount];
}
    
@end