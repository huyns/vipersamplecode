//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountContainerProtocols.h"
#import "AccountContainerPresenter.h"

@interface AccountContainerViewController : UIViewController <AccountContainerViewProtocol>

@property (nonatomic, strong) AccountContainerPresenter *presenter;
@property (weak, nonatomic) IBOutlet UIView *accountListContainerView;
@property (weak, nonatomic) IBOutlet UIView *accountDetailContainerView;

@end
