//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AccountContainerDataManagerOutputProtocol;
@protocol AccountContainerServiceManagerOutputProtocol;
@protocol AccountContainerInteractorOutputProtocol;
@protocol AccountContainerInteractorInputProtocol;
@protocol AccountContainerViewProtocol;
@protocol AccountContainerPresenterProtocol;
@protocol AccountContainerDataManagerInputProtocol;
@protocol AccountContainerServiceManagerInputProtocol;
@protocol AccountContainerWireFrameInputProtocol;

typedef void (^CompletionBlock)(NSError **error);

#pragma mark AccountContainerViewProtocol
@protocol AccountContainerViewProtocol
@required
@property (nonatomic, strong) id <AccountContainerPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark AccountContainerPresenterProtocol
@protocol AccountContainerPresenterProtocol
@required
@property (nonatomic, weak) id <AccountContainerViewProtocol> view;
@property (nonatomic, strong) id <AccountContainerInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <AccountContainerWireFrameInputProtocol> wireFrame;
/**
 * Add here your methods for communication VIEWCONTROLLER/WIREFRAME -> PRESENTER
 */
-(void) showAccountList:(UIView *) containerView;
-(void) showAccount:(id) account;
-(void) dismissView;
-(void) addAccount;
@end

#pragma mark AccountContainerWireFrameInputProtocol
@protocol AccountContainerWireFrameInputProtocol <NSObject>
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 */
-(void) presentAccountListInContainerView:(id) view;
-(void) presentAccount:(id) account;
-(void) dismissView;
-(void) presentAddAccountView;
@end

#pragma mark AccountContainerInteractorInputProtocol
@protocol AccountContainerInteractorInputProtocol
@required
@property (nonatomic, weak) id <AccountContainerInteractorOutputProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end

#pragma mark AccountContainerInteractorOutputProtocol
@protocol AccountContainerInteractorOutputProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

#pragma mark AccountContainerDataManagerInputProtocol
@protocol AccountContainerDataManagerInputProtocol
@property (nonatomic, weak) id <AccountContainerDataManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AccountContainerDataManagerOutputProtocol
@protocol AccountContainerDataManagerOutputProtocol
@property (nonatomic, strong) id <AccountContainerDataManagerInputProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
@end

#pragma mark AccountContainerServiceManagerInputProtocol
@protocol AccountContainerServiceManagerInputProtocol
@property (nonatomic, weak) id <AccountContainerServiceManagerOutputProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark AccountContainerServiceManagerOutputProtocol
@protocol AccountContainerServiceManagerOutputProtocol
@property (nonatomic, strong) id <AccountContainerServiceManagerInputProtocol> serviceManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */

@end
