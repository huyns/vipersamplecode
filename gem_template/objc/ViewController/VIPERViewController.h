//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERProtocols.h"
#import "VIPERPresenter.h"

@interface VIPERViewController : UIViewController <VIPERViewProtocol>

@property (nonatomic, strong) VIPERPresenter *presenter;
@end
