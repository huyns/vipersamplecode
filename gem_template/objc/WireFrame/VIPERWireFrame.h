//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERProtocols.h"
#import "VIPERViewController.h"
#import "VIPERDataManager.h"
#import "VIPERInteractor.h"
#import "VIPERPresenter.h"
#import "VIPERWireframe.h"
#import "VIPERViewModel.h"
#import <UIKit/UIKit.h>

@interface VIPERWireFrame : NSObject <VIPERWireFrameProtocol>

@property (nonatomic, weak) VIPERPresenter *presenter;
- (VIPERViewController *)viewController;
- (void)presentModuleFrom:(id)fromView;
- (void)pushToModuleFrom:(id)fromView;
@end
