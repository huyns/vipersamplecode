//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERWireFrame.h"

@interface VIPERWireFrame()
@end

@implementation VIPERWireFrame
@synthesize presenter = _presenter;

- (void)presentModuleFrom:(id)fromView {
    VIPERViewController *viewController = [self viewController];
    /// perform present |viewController| below
}

- (void)pushToModuleFrom:(UIViewController *)fromView {
    VIPERViewController *viewController = [self viewController];
    [fromView.navigationController pushViewController:viewController animated:YES];
}

#pragma mark private methods
- (VIPERViewController *)viewController {
    if (self.presenter) {
        return (VIPERViewController *)self.presenter.view;
    } else {
        VIPERViewController *viewcontroller = [VIPERViewController new];
        
        VIPERPresenter *presenter = [VIPERPresenter new];
        id<VIPERInteractorProtocol, VIPERDataManagerInteractorProtocol> interactor = [VIPERInteractor new];
        id<VIPERDataManagerProtocol> dataManager = [VIPERDataManager new];
        VIPERViewModel *viewModel = [VIPERViewModel new];
        
        presenter.wireFrame = self;
        presenter.view = viewcontroller;
        presenter.viewModel = viewModel;
        presenter.interactor = interactor;
        
        viewcontroller.presenter = presenter;
        
        interactor.presenter = presenter;
        interactor.dataManager = dataManager;
        
        dataManager.interactor = interactor;
        
        self.presenter = presenter;
        return viewcontroller;
    }
}

#pragma mark VIPERWireFrameProtocol

@end
