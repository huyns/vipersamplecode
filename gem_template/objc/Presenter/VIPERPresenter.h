//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERProtocols.h"

@class VIPERWireFrame;

@interface VIPERPresenter : NSObject <VIPERPresenterProtocol, VIPERViewPresenterProtocol, VIPERWireFramePresenterProtocol, VIPERInteractorPresenterProtocol>
// Properties
@end
