//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERPresenter.h"
#import "VIPERWireframe.h"

@interface VIPERPresenter()

@end

@implementation VIPERPresenter
@synthesize view = _view;
@synthesize interactor = _interactor;
@synthesize wireFrame = _wireFrame;
@synthesize viewModel = _viewModel;

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark VIPERViewPresenterProtocol

#pragma mark VIPERWireFramePresenterProtocol

#pragma mark VIPERInteractorPresenterProtocol

@end
