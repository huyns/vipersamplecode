//
//  VIPERViewModel.h
//  ViperSampleCode
//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import "VIPERProtocols.h"

@interface VIPERViewModel : NSObject <VIPERViewModelProtocol>

@end
