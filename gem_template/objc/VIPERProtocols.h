//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol VIPERViewProtocol;
@protocol VIPERViewModelProtocol;
@protocol VIPERPresenterProtocol;
@protocol VIPERViewPresenterProtocol;
@protocol VIPERWireFramePresenterProtocol;
@protocol VIPERWireFrameProtocol;
@protocol VIPERInteractorProtocol;
@protocol VIPERInteractorPresenterProtocol;
@protocol VIPERDataManagerProtocol;
@protocol VIPERDataManagerInteractorProtocol;

#pragma mark VIPERViewProtocol
@protocol VIPERViewProtocol
@required
@property (nonatomic, strong) id presenter;
/**
 * Add here your methods for communication PRESENTER -> VIEWCONTROLLER
 */
@end

#pragma mark VIPERViewModelProtocol
@protocol VIPERViewModelProtocol
/**
 * Add here your setter methods for communication PRESENTER -> VIEWMODEL
 * And getter method for communication       VIEW/PRESENTER -> VIEWMODEL
 */
@end

#pragma mark VIPERPresenterProtocol
@protocol VIPERPresenterProtocol
@required
@property (nonatomic, weak) id<VIPERViewProtocol> view;
@property (nonatomic, strong) id<VIPERInteractorProtocol> interactor;
@property (nonatomic, strong) id<VIPERWireFrameProtocol> wireFrame;
@property (nonatomic, strong) id<VIPERViewModelProtocol> viewModel;
@end

#pragma mark VIPERViewPresenterProtocol
@protocol VIPERViewPresenterProtocol
/**
 * Add here your methods for communication VIEW -> PRESENTER
 */
@end

#pragma mark VIPERWireFrameProtocol
@protocol VIPERWireFrameProtocol <NSObject>
@required
@property (nonatomic, weak) id<VIPERWireFramePresenterProtocol> presenter;
/**
 *  Add here your methods for communication PRESENTER -> WIREFRAME
 *  Methods for communication from an other WIREFRAME -> WIREFRAME
 */
@end

#pragma mark VIPERWireFramePresenterProtocol
@protocol VIPERWireFramePresenterProtocol
/**
 * Add here your methods for communication WIREFRAME -> PRESENTER
 */
@end

#pragma mark VIPERInteractorProtocol
@protocol VIPERInteractorProtocol
@required
@property (nonatomic, weak) id<VIPERInteractorPresenterProtocol> presenter;
/**
 * Add here your methods for communication PRESENTER -> INTERACTOR
 */
@end

#pragma mark VIPERInteractorPresenterProtocol
@protocol VIPERInteractorPresenterProtocol
/**
 * Add here your methods for communication INTERACTOR -> PRESENTER
 */
@end

#pragma mark VIPERDataManagerProtocol
@protocol VIPERDataManagerProtocol
@property (nonatomic, weak) id<VIPERDataManagerInteractorProtocol> interactor;
/**
 * Add here your methods for communication INTERACTOR -> DATAMANAGER
 */
@end

#pragma mark VIPERDataManagerInteractorProtocol
@protocol VIPERDataManagerInteractorProtocol
@property (nonatomic, strong) id<VIPERDataManagerProtocol> dataManager;
/**
 * Add here your methods for communication DATAMANAGER -> INTERACTOR
 */
@end
