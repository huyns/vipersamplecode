//
//  Created by GEM on 8/29/16.
//  Copyright © 2016 GEM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VIPERProtocols.h"

@interface VIPERServiceManager : NSObject <VIPERServiceManagerProtocol>
// Properties
@property (nonatomic, weak) id<VIPERServiceManagerInteractorProtocol>interactor;
@end